---
title: "Debian Project Funding"
date: 2021-10-16T14:50:03-04:00
description: "Propose a project.  Get status on existing projects."
---

## Debian Project Funding

In addition to directly supporting customers through a range of service offerings, Freexian also funds and manages projects that help to improve Debian.  This service is intended to allow internal Debian teams and external customers alike to leverage Freexian's project management capabilities and existing relationships with contributors to Debian in order to bring about improvements to Debian.

### Proposing a Project

For additional information about proposing a project to be funded under this initiative, [click here](https://salsa.debian.org/freexian-team/project-funding).

### Status of Proposed Projects

For additional information about the status of proposed projects under this initiative, [click here](/project-funding/projects/).
