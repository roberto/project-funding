# Project Data

| Title | A New Project |
| ----- | ----- |
| **Funding source** | LTS or Freexian |
| **Submitter** | John Doe <jdoe@example.com>, (IRC: jdoe) |
| **Executor** | To be found |
| **Reviewer** | To be found |
| **Proposal date** | YYYY-MM-DD |
| **Approval date** | (insert date when approved) |
| **Request for bids** | (insert link to eventual request) |
| **Project management issue** | (insert link when project starts) |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Rationale

Explain why you are submitting this project. What benefits do you expect
out of its realization?

# Description

A single sentence that describes this excellent new project.

## Project Details

A thorough and detailed description of the project, the work to be done, etc.

## Completion Criteria

List of one or more specific and measurable criteria that indicate successful
project completion.

* Criterion 1
* Criterion 2

## Estimated Effort

An estimate of the effort involved.  E.g., this project will require up to
8 hours of work. For larger projects, you might want to propose some
intermediary milestones, each with its own work estimation.

This estimation sets expectations for the bids to be submitted and helps
to evaluate the cost of the collected bids.

----------------------------------------

# Submitter Bid

If the submitter want to be the executor, they can submit their own bid
in this section. Otherwise this section should be dropped.
